### [RavenPack News Analytics](https://r4.tun.to/ravenpack.zip)
**Format:** SAS dataset  
**Source:** WRDS  

### [Zacks Investment Research](https://r4.tun.to/zacks_research.zip)
**Format:** SAS dataset  
**Source:** WRDS  
Covers 01/01/1978 until 05/22/2020. Contains analyst forecasts, revisions, price targets and recommendation history for all US equities.  
[Variable Definitions](https://r4.tun.to/zacks_definitions.txt)
