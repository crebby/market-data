### [GovPX US Treasury Benchmark](https://r4.tun.to/govpx_treasury.zip) ###
**Format:** SAS dataset  
[Variable Definitions](https://r4.tun.to/govpx_definitions.txt)  
GovPX Treasury prices are "flat prices". They do not include accrued interest. GovPX no longer calculates Aggregate Volume. It is not available after March 2001. Due to the Hurricane Sandy weather event, there was no activity the evening of October 29, 2012 into October 30, 2012 as the market was closed. As a result, there is missing data for October 30, 2012 in the GovPX dataset.
