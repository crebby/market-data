# Market Data

A growing collection of high quality market data. Contributors are welcome to add sources.

---

## More Sources
**[NASDAQ and NYSE stocks histories](https://www.kaggle.com/qks1lver/nasdaq-and-nyse-stocks-histories/home)**  
Full daily historic prices of 5800+ stocks on NASDAQ and NYSE listings (kaggle)  


 